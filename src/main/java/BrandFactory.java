public class BrandFactory extends AbstractFactory {
    @Override
    Brand getBrand(String BrandType) {
        if (BrandType.equalsIgnoreCase("ADIDAS")){
            return new Adidas();
        }else if(BrandType.equalsIgnoreCase("BOSS")) {
            return new Boss();
        }
        return null;
    }
}
