public  class Adidas implements Brand {
    @Override
    public  String shirt() {
        return "adidas paita";
    }
    @Override
    public  String cap() {
        return "adidas lippa";
    }
    @Override
    public String shoes() {
        return "adidas kengät";
    }
    @Override
    public String shorts() {
        return "adidas shortsit";

    }
}
