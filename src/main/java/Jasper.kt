object Jasper {
    @JvmStatic
    fun main(args: Array<String>) {

        val brandFactory = FactoryProducer.getFactory()
        var brand = brandFactory.getBrand("ADIDAS")
        println("Jasper opiskelu:")
        println(brand.shirt())
        println(brand.cap())
        println(brand.shoes())
        println(brand.shorts())

        println("Valmistunut Jasper:")
        brand = brandFactory.getBrand("BOSS")
        println(brand.shirt())
        println(brand.cap())
        println(brand.shoes())
        println(brand.shorts())
    }
}