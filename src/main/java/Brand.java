public interface Brand {
    abstract String shirt();
    abstract  String cap();
    abstract String shoes();
    abstract String shorts();
}
